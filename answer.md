answer
======================

[e-Stat 政府統計の総合窓口](https://www.e-stat.go.jp/municipalities/number-of-municipalities)

|項目　| 数 |
|------|----|
|都　　|1|
|道　　|1|
|府　　|2|
|県　　|43|
|市　　|772|
|区総数|198|
|特別区|23|
|区　　|175|
|町　　|743|
|村　　|183|