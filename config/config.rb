require 'yaml'

YAML_FILE = File.join(__dir__, './config.yml')
YAML_DATA = YAML.load_file(YAML_FILE)

CONF_GENERAL = YAML_DATA['general']
CONF = YAML_DATA[CONF_GENERAL['mode']]

CONF_LOG = CONF['putlog']

