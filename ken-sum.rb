require 'csv'
require 'nkf'
require_relative './lib/putlog'
require_relative './config/config'

# options
data_source = './data/KEN_ALL.CSV'

valid = {
  to: /都\Z/,
  dou: /道\Z/,
  hu: /府\Z/,
  ken: /県\Z/,
  shi: /市\Z/,
  ku: /区\Z/,
  cho: /町\Z/,
  son: /村\Z/
}

# init
log_dir = File.join(CONF_LOG['path'], CONF_GENERAL['name'])
log = Putlog.new(CONF_LOG['mode'], CONF_LOG['level'], log_dir)

aggregate = {
  to: [],
  dou: [],
  hu: [],
  ken: [],
  shi: [],
  ku: [],
  cho: [],
  son: [],
  exeption: []
}

log.info("##### START PROCESS #{CONF_GENERAL['name']} #####")
log.debug("source file: #{data_source}")

# Character code estimation
enc = NKF.guess(File.read(data_source, 1000)).name
log.debug("csv character code: #{enc}")

prefecture = nil
municipalities = nil

log.info("load #{data_source} and count...")
CSV.foreach(data_source, encoding: "#{enc}:UTF-8") do |row|
  log.debug("line: #{row}")
  if prefecture != row[6]
    prefecture = row[6]
    log.debug("valid prefecture: #{prefecture}")
    case prefecture
    when valid[:to]
      aggregate[:to].push(prefecture)
    when valid[:dou]
      aggregate[:dou].push(prefecture)
    when valid[:hu]
      aggregate[:hu].push(prefecture)
    when valid[:ken]
      aggregate[:ken].push(prefecture)
    else
      aggregate[:exeption].push(prefecture)
    end
  end

  if municipalities != row[7]
    municipalities = row[7]
    log.debug("valid municipalities: #{municipalities}")
    case municipalities
    when valid[:shi]
      aggregate[:shi].push(municipalities)
    when valid[:ku]
      aggregate[:ku].push(municipalities)
    when valid[:cho]
      aggregate[:cho].push(municipalities)
    when valid[:son]
      aggregate[:son].push(municipalities)
    else
      aggregate[:exeption].push(municipalities)
    end
  end
end

puts '項目| 数 '
puts '----+---------'
puts " 都 | #{aggregate[:to].length}"
puts " 道 | #{aggregate[:dou].length}"
puts " 府 | #{aggregate[:hu].length}"
puts " 県 | #{aggregate[:ken].length}"
puts " 市 | #{aggregate[:shi].length}"
puts " 区 | #{aggregate[:ku].length}"
puts " 町 | #{aggregate[:cho].length}"
puts " 村 | #{aggregate[:son].length}"

unless aggregate[:exeption].empty?
  puts "異常| #{aggregate[:exeption].length}"
  log.warn('exception has occurred, some rows are not sorted.')
  log.warn(aggregate[:exeption])
end

log.info("##### SUCCEED PROCESS #{CONF_GENERAL['name']} #####")
