require 'logger'
require 'fileutils'

class Putlog
  def initialize(mode='STDOUT', level='PROD', path='./', log_name='app.log')
    log_path = "#{path}/#{log_name}"
    lotation = 'monthly'
    @mode = mode
    @level = level
    if @mode == 'LOGFILE' || @mode == 'TEE'
      FileUtils.mkpath(path) unless Dir.exist?(path)
    end
    if @level == 'PROD'
      case @mode
        when 'STDOUT'
          @log = Logger.new(STDOUT)
          @log.level = Logger::INFO
        when 'LOGFILE'
          @log = Logger.new(log_path, lotation)
          @log.level = Logger::INFO
        when 'TEE'
          @log_stdout = Logger.new(STDOUT)
          @log_output = Logger.new(log_path, lotation)
          @log_stdout.level = Logger::INFO
          @log_output.level = Logger::INFO
      end
    end
    if @level == 'DEBUG'
      case @mode
        when 'STDOUT'
          @log = Logger.new(STDOUT)
        when 'LOGFILE'
          @log = Logger.new(log_path, lotation)
        when 'TEE'
          @log_stdout = Logger.new(STDOUT)
          @log_output = Logger.new(log_path, lotation)
      end
    end
  end

  def unknown(message)
    return if @mode == 'NONE'
    if @mode == 'TEE'
      @log_stdout.unknown(message)
      @log_output.unknown(message)
    else
      @log.unknown(message)
    end
  end

  def fatal(message)
    return if @mode == 'NONE'
    if @mode == 'TEE'
      @log_stdout.fatal(message)
      @log_output.fatal(message)
    else
      @log.fatal(message)
    end
  end

  def error(message)
    return if @mode == 'NONE'
    if @mode == 'TEE'
      @log_stdout.error(message)
      @log_output.error(message)
    else
      @log.error(message)
    end
  end

  def warn(message)
    return if @mode == 'NONE'
    if @mode == 'TEE'
      @log_stdout.warn(message)
      @log_output.warn(message)
    else
      @log.warn(message)
    end
  end

  def info(message)
    return if @mode == 'NONE'
    if @mode == 'TEE'
      @log_stdout.info(message)
      @log_output.info(message)
    else
      @log.info(message)
    end
  end

  def debug(message)
    return if @mode == 'NONE'
    if @mode == 'TEE'
      @log_stdout.debug(message)
      @log_output.debug(message)
    else
      @log.debug(message)
    end
  end
end
